import re
import json, datetime
import requests
from multiprocessing import Process
class pogoda():
    def __init__(self, city_name, humidity, pressure, temp, wind):
        self.city_name = f'В городе {city_name} \n'
        self.humidity = f'Влажность воздуха: {humidity}%\n'
        self.pressure = f'Давление: {pressure} мм.р.т.\n'
        self.temp = f'Температура воздуха составит: {temp} C° \n'
        self.wind = f'Ветер: {wind}м/с\n'
cities = []
with open("cities.txt", "r", encoding="utf-8") as f:
    for line in f:
        if line != "Список городов:\n":
            cities.append(re.findall(r'[a-zA-Z]+',line))
for i in cities:
    cities[cities.index(i)] = ' '.join(cities[cities.index(i)])

def data_collection(name):
    openweatherAPI = '0fb610dab7456bc44dbdde2ddba9be71'
    r = requests.get(f'https://api.openweathermap.org/data/2.5/weather?q={name}&appid={openweatherAPI}&units=metric')
    r_text=json.loads(r.text)
    city = pogoda(r_text['name'], r_text['main']['humidity'], r_text['main']['pressure'], r_text['main']['temp'],r_text['wind']["speed"])
    print(city.city_name,city.humidity,city.pressure,city.temp, city.wind)
if __name__ == '__main__':
    for i in cities: 
        locals()['p' + str(cities.index(i))] = Process(target=data_collection, args=(''.join(cities[cities.index(i)]),))
        locals()['p' + str(cities.index(i))].start()
    for i in cities:
        locals()['p' + str(cities.index(i))].join()


